import { Component, OnInit } from '@angular/core';
import { Act } from '../model/act.model';
import { ActService } from '../services/act.service';
import { Lect } from '../model/lect.model';
import { LectService } from '../services/lect.service';
import { Section } from '../model/section.model';
import { SectionService } from '../services/section.service';
import { ModalController, PopoverController } from '@ionic/angular';
import { SidemenuPage } from '../sidemenu/sidemenu.page';
import { Router } from '@angular/router';
import { FormArray, FormGroup, FormBuilder } from '@angular/forms';
import { Section5InPage } from '../section5-in/section5-in.page';


@Component({
  selector: 'app-section5',
  templateUrl: './section5.page.html',
  styleUrls: ['./section5.page.scss'],
})

export class Section5Page implements OnInit {

  actList : Act[] = new Array<Act>();
  lectList : Lect[] = new Array<Lect>();
  sectionList : Section[] = new Array<Section>();
  sectionID : Section;
  topicID:any;
  topicName :any;
  // section: Section ='';
  selectedSection:any={id : 5, name: "Pengurusan Kursus II"};
  scheduleID : number = 0;
  s5Form: FormGroup;
  s5: FormArray;

  constructor(private actSvc: ActService, private router: Router, private modalController: ModalController, private formBuilder : FormBuilder, private lectSvc: LectService, private sectionSvc: SectionService, public popoverController: PopoverController) {}

  ngOnInit(){
    // this.s5Form = this.formBuilder.group({
    //   s5: this.formBuilder.array([])
    // });

    // this.actSvc.getListOfAct().subscribe(
    //   data=>{
    //     this.actList = data;
    //     for(let item of this.actList){
    //       this.s5 = this.s5Form.get('s5') as FormArray;
    //       this.s5.push(this.createItem(item));
    //       this.scheduleID = item.scheduleID;
    //     }
    //   }
    // )   

    // createItem(item: Act): FormGroup {
    //   return this.formBuilder.group({
    //     id:[item.topicNo],
    //     topic: [item.topicName],
    //     conFlg: [item.continueFlg]
    //   });
    // }

    this.actSvc.getListOfAct().subscribe(
      data=>{
        this.actList = data;
      }
    )

    this.lectSvc.getListOfLect().subscribe(
      data=>{
        this.lectList = data;
      }
    )
    this.sectionSvc.getListOfSection().subscribe(
      data=>{
        this.sectionList = data;
        /*this.sectionID = this.sectionList[4];
        this.topicID =this.sectionList[4].topicID;
        this.topicName = this.sectionList[4].topicName;*/

      }
    )
  }

  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: SidemenuPage,
      cssClass: 'my-custom-class',
      //componentProps: 
      event: ev,
      translucent: true
    });
    return await popover.present();
  }

  home(){
    this.router.navigateByUrl("/home");
  }

  // async openModalWithData() {
  //   const modal = await this.modalController.create({
  //     component: Section5InPage
  //     // componentProps: {
  //     //   lunch: this.lunch
  //     // }
  //   });
  //   modal.onWillDismiss().then(dataReturned => {
  //     // trigger when about to close the modal
  //     //this.dinner = dataReturned.data;
  //     //console.log('Receive: ', this.dinner);
  //     console.log('Receive: ');
  //   });
  //   return await modal.present().then(_ => {
  //     // triggered when opening the modal
  //     console.log('Sending: ', this.lunch);
  //   });
  // }


}