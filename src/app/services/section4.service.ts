import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Topic } from '../model/topic.model';

@Injectable({
    providedIn: 'root'
})
export class Section4Service {
 
    constructor(
        private httpClient: HttpClient
    ){}

    getListOfSection4():Observable<any>{
        return this.httpClient.get("/assets/dummydata/section4.json")
    }
    getTopicById(id : string):Observable<Topic>{
        return this.httpClient.get<Topic>("/assets/dummydata/singletopic.json")
    }
}