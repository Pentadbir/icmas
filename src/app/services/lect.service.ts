import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Lect } from "../model/lect.model"

@Injectable({
    providedIn: 'root'
})
export class LectService {
 
    constructor(
        private httpClient: HttpClient
    ){}

    getListOfLect():Observable<any>{
        return this.httpClient.get("/assets/dummydata/listoflecturer.json")
    }
}