export class Section6 {
    public valTopicID : number;
    public scheduleID : number;
    public remarks1 : string;
    public remarks2 : string;

    public setValTopicID(valTopicID:number){
        this.valTopicID = valTopicID;
    }
    public setScheduleID(scheduleID:number){
        this.scheduleID = scheduleID;
    }    
    public setRemarks1(remarks1:string){
        this.remarks1 = remarks1;
    }
    public setRemarks2(remarks2:string){
        this.remarks2 = remarks2;
    }
}