import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { FormArray, FormBuilder, FormGroup} from '@angular/forms';
import { AlertController, PopoverController } from '@ionic/angular';
import { SidemenuPage } from '../sidemenu/sidemenu.page';
import { Section3 } from '../model/section3.model';
import { Section3Service } from '../services/section3.service';
import { Section } from '../model/section.model';
import { SectionService } from '../services/section.service';
import { PickerController } from "@ionic/angular";

@Component({
  selector: 'app-section3',
  templateUrl: './section3.page.html',
  styleUrls: ['./section3.page.scss'],
})
export class Section3Page implements OnInit {

  section3List : Section3[] = new Array<Section3>();

  topicID:any;
  topicName :any;
  sectionList: Section[];
  section: Section;
  private valueScore : number = 7;
  selectedSection:any={id : 3, name: "Penyelarasan Kursus"};


  scheduleID : number = 0;
  public isSubmitted: boolean = false;
  public isRequired: boolean = false;
  private score : number = 7;

  scoreList =[];
  s3Form: FormGroup;
  s3: FormArray;
  
  constructor(private section3Service: Section3Service, private formBuilder: FormBuilder, public alertController: AlertController, private sectionService: SectionService,
    public popoverController: PopoverController, private router: Router,private pickerController: PickerController) { }
   
    ngOnInit(){    
      this.s3Form = this.formBuilder.group({
        s3: this.formBuilder.array([]),
    });
     
      this.section3Service.getListOfSection3().subscribe(
        data=>{
          this.section3List = data;
          for(let item of this.section3List){
            this.s3 = this.s3Form.get('s3') as FormArray;
            this.s3.push(this.createItem(item));
            //this.scheduleID = item.scheduleID;
            //console.log(data);
          }
          // this.initForm();
        }
      )
     
      this.sectionService.getListOfSection().subscribe(
        data=>{
          this.sectionList = data;
          //this.section = this.sectionList[id];
        }
      )
    }
    
    createItem(item: Section3): FormGroup {
      return this.formBuilder.group({
        id:[item.valSubTopicID],
        section3: [item.valSubTopicName],
        valueScore: 7,
        remarks: ['']  
      });
    }

    get errorControl(){
      return this.s3Form.controls;
    }

    changeRequiredStatus() {
      this.isRequired= true;
    }

    home(){
      this.router.navigateByUrl("/home");
    }

    async presentPopover(ev: any) {
      const popover = await this.popoverController.create({
        component: SidemenuPage,
        cssClass: 'my-custom-class',
        //componentProps:
        event: ev,
        translucent: true
      });
      return await popover.present();
    };

  submit(){
    this.isSubmitted = true;
    this.isRequired = true;
    if (!this.s3Form.valid){
       console.log('Sila isi ulasan...');
       return false;
     }
    else{
      let control = this.s3Form.get('s3') as FormArray;
      let section3List : Section3[] = new Array<Section3>();
  
      for(let i=0; i < control.length; i++){
        let val : Section3 = new Section3();
        
        val.valSubTopicID = control.controls[i].get('id').value;
        
        // if ((control.controls[i].get('valueScore').value != 0) && (control.controls[i].get('valueScore').value <=4))
        // { control.controls[i].get('remarks').value !="";
        //   return false;
        //   //console.log(control.controls[i].get('remarks').value);   
        // }
        val.score = control.controls[i].get('valueScore').value;
        val.remarks = control.controls[i].get('remarks').value;

        section3List.push(val);
      }
      console.log(section3List);  
      this.router.navigateByUrl("/section4");  
     }
    }

    async presentAlertConfirm() {
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Pengesahan',
        message: 'Anda pasti untuk <strong>menghantar penilaian</strong>?',
        buttons: [
          {
            text: 'Tidak',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Ya',
            handler: () => {
              this.isSubmitted = true;
              if (!this.s3Form.valid){
                 console.log('Sila isi ruangan yang  diperlukan.');
                 alert.dismiss();
                 return false;               
               }
               else{
                console.log(this.s3Form.value);  
                this.router.navigateByUrl("/section4");  
               }
              console.log('Confirm Okay');
            }
          }
        ]
      });
      await alert.present();
    }


  }