import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class Section1Service {

    section1List : Section1Service[];
    
    constructor(
        private httpClient: HttpClient
    ){}

    getListOfSection1():Observable<any>{
        return this.httpClient.get("/assets/dummydata/listofsection1.json")
    }
}