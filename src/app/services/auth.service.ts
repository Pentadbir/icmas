import { INJECTOR, Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../model/user.model';
import { serverEnv } from 'src/environments/server-env';
import { TokenService } from './token.service';

const httpOption = {
    headers : new HttpHeaders({
        // 'Content-Type': 'application/x-www-form-urlencoded',
        'Content-Type': 'application/json'
    })
}

@Injectable({

    providedIn:'root'
})

export class AuthService{
    
    globalUrl=serverEnv.apiUrl; 
    apiLogin=this.globalUrl + "/login";
    


    logout() {
      throw new Error("Method not implemented.");
    }

    constructor(
        private httpClient : HttpClient,
        private tokenSvc : TokenService 
    ){}

    getUser():Observable<any>{
        return this.httpClient.get("/assets/dummydata/listofuser.json");
    }

    
    // login(user : User):Observable<any>{
    //     //return this.httpClient.get("/assets/dummydata/listofuser.json")
    //     //return this.httpClient.post("https://reqres.in/api/login",peserta);
    //     //return this.httpClient.post("https://reqres.in/api/login",user)
    //     //return this.httpClient.post("http://localhost/myICMASPenilaian/src/app/api/login.php/",user)
    // }

    login(newUser:User):Observable<any>{

        return this.httpClient.post(this.apiLogin,JSON.stringify({
            username : newUser.newICNo,
            password : newUser.passwd
        }),httpOption)

    }

    setToken(token){
        localStorage.setItem(this.tokenSvc.TOKEN_KEY,token);
        localStorage.setItem("isLoggedIn","true")
    }

    getToken(){
        return localStorage.getItem(this.tokenSvc.TOKEN_KEY)
    }

    removeToken(){
        localStorage.removeItem(this.tokenSvc.TOKEN_KEY)
        localStorage.removeItem("isLoggedIn")
    }

    // register(user: User):Observable<any>{
    //     return this.httpClient.post("https://reqres.in/api/register", user)
    // }
}