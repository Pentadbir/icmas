export class Coordinator {
    public penyelarasID : number;
    public userID : string;
    public ICNo : string;
    public name : string;
   
    public setPenyelarasID(penyelarasID:number){
        this.penyelarasID = penyelarasID;
    }
    public setUserID(userID:string){
        this.userID = userID;
    }
    public setICNo(ICNo:string){
        this.ICNo = ICNo;
    }    
    public setName(name:string){
        this.name = name;
    }
}