import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class ActService {

    constructor(
        private httpClient: HttpClient
    ){}

    getListOfAct():Observable<any>{
        return this.httpClient.get("/assets/dummydata/listofact.json")
    }
}