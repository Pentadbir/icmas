import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Section4PageRoutingModule } from './section4-routing.module';

import { Section4Page } from './section4.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Section4PageRoutingModule, 
    ReactiveFormsModule
  ],
  declarations: [Section4Page]
})
export class Section4PageModule {}
