import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Section5InPage } from './section5-in.page';

const routes: Routes = [
  {
    path: '',
    component: Section5InPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Section5InPageRoutingModule {}
