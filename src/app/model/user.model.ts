export class User {
    public username : string;
    public name : string;
    public first_name : string;
    public last_name : string;
    public avatar : string;
    public email: string;
    public password : string;
    public gender : string;
    public newICNo : string;
    public passwd : string;

    public setUserName(username:string){
        this.username = username;
    }
    public setName(name:string){
        this.name = name;
    }
    public setEmail(email:string){
        this.email = email;
    }
    public setPassword(password:string){
        this.password = password;
    }
    public setGender(gender:string){
        this.gender = gender;
    }
    public setAvatar(avatar:string){
        this.avatar = avatar;
    }
    public setFirst_Name(first_name:string){
        this.first_name = first_name;
    }
    public setLast_Name(last_name:string){
        this.last_name = last_name;
    }
}