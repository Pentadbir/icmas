import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Section3Page } from './section3.page';

const routes: Routes = [
  {
    path: '',
    component: Section3Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Section3PageRoutingModule {}
