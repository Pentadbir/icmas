import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Section3PageRoutingModule } from './section3-routing.module';

import { Section3Page } from './section3.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Section3PageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [Section3Page]
})
export class Section3PageModule {}
