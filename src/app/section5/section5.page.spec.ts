import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Section5Page } from './section5.page';

describe('Section5Page', () => {
  let component: Section5Page;
  let fixture: ComponentFixture<Section5Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Section5Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Section5Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
