import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Section3Page } from './section3.page';

describe('Section3Page', () => {
  let component: Section3Page;
  let fixture: ComponentFixture<Section3Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Section3Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Section3Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
