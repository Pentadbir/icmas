export class Objective {
    public scheduleobj_id : number;
    public scheduleobj_text : string;
    public course_id : string;
    public scheduleID : number;
    public score : number;
    public remarks : string;

    public setScheduleObjID(scheduleobj_id:number){
        this.scheduleobj_id = scheduleobj_id;
    }
    public setScheduleObjName(scheduleobj_text:string){
        this.scheduleobj_text = scheduleobj_text;
    }
    public setScheduleID(scheduleID:number){
        this.scheduleID = scheduleID;
    }    
    public setScore(score:number){
        this.score = score;
    }
    public setRemarks(remarks:string){
        this.remarks = remarks;
    }

    public setCourseID(course_id:string){
        this.course_id = course_id;
    }

}