export class Topic {
    public topicID : number;
    public topicName : string;
    public topicCode : string;
    public maxGrd : number;
    public typeVal : string;

    public setTopicID(topicID:number){
        this.topicID = topicID;
    }
    public setTopicName(topicName:string){
        this.topicName = topicName;
    }
    public setTopicCode(topicCode:string){
        this.topicCode = topicCode;
    }    
    public setMaxGrd(maxGrd:number){
        this.maxGrd = maxGrd;
    }
    public setTypeVal(typeVal:string){
        this.typeVal = typeVal;
    }
}