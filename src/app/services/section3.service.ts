import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Topic } from '../model/topic.model';

@Injectable({
    providedIn: 'root'
})
export class Section3Service {
 
    constructor(
        private httpClient: HttpClient
    ){}

    getListOfSection3():Observable<any>{
        return this.httpClient.get("/assets/dummydata/section3.json")
    }
    getTopicById(id : string):Observable<Topic>{
        return this.httpClient.get<Topic>("/assets/dummydata/singletopic.json")
    }
}