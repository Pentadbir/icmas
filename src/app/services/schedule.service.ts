import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { serverEnv } from 'src/environments/server-env';
import { AuthService } from './auth.service';
import { TokenService } from './token.service';


@Injectable({
    providedIn: 'root'
})



export class ScheduleService {
    globalUrl=serverEnv.apiUrl; 
    apiHome=this.globalUrl + "/home";

    getListOfTopic() {
      throw new Error("Method not implemented.");
    }
 
    constructor(
        private httpClient: HttpClient,
        private _token : TokenService
    ){}

    getHome():Observable<any>{
        return this.httpClient.get(this.apiHome,this._token.jwtBearer());
    }

    getListOfSchedule():Observable<any>{
        return this.httpClient.get("/assets/dummydata/listofschedule.json");
    }
 
}