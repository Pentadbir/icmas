export class subTopic {
    public topicID :number
    public subTopicID : number;
    public subTopicName : string;
    public topicCode : string;
    public typeVal : string;

    public setTopicID(topicID:number){
        this.topicID = topicID;
    }
    public setSubTopicID(subTopicID:number){
        this.subTopicID = subTopicID;
    }
    public setSubTopicName(subTopicName:string){
        this.subTopicName = subTopicName;
    }
    public setTypeVal(typeVal:string){
        this.typeVal = typeVal;
    }
}