import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Section4Page } from './section4.page';

const routes: Routes = [
  {
    path: '',
    component: Section4Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Section4PageRoutingModule {}
