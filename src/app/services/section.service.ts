import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Section } from '../model/section.model';
import { serverEnv } from 'src/environments/server-env';
import { TokenService } from './token.service';


@Injectable({
    providedIn: 'root'
})
export class SectionService {

    globalUrl=serverEnv.apiUrl; 
    apiSection=this.globalUrl + "/section";

    sectionList : SectionService[];
    
    constructor(
        private httpClient: HttpClient,
        private _token : TokenService
    ){}

    getListOfSection():Observable<any>{
       
        return this.httpClient.get(this.apiSection,this._token.jwtBearer());
    }

    getSectionById(id : string):Observable<Section>{
        return this.httpClient.get<Section>("/assets/dummydata/singlesection.json")
    }
    getSectionByID(id : string):Observable<any>{
        return this.httpClient.get("/assets/dummydata/singlesection.json")
    }
    // getScheduleFromApi(scheduleid: number):Observable<any>{
    //     return this.httpClient.get("https://reqres.in/api/users/"+scheduleid)
    //     //return this.httpClient.get("http://localhost/myICMASPenilaian/src/app/api/login.php/"+user_id)

    // }
}