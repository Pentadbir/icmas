import { Objective } from './obj.model';
import { Act } from './act.model';

export class Schedule{
    public scheduleID : number;
    public scheduleCode : string;
    public scheduleName : string;
    public startDt : string;
    public endDt : string;
    public penyelarasID : number;
    public urusetiaID : number;
    public courseID : number;
    public ObjList : Objective[];
    public scheduleActList : Act[];
    public status : string;
    
    public setScheduleID(scheduleID:number){
        this.scheduleID = scheduleID;
    }
    public setScheduleCode(scheduleCode:string){
        this.scheduleCode = scheduleCode;
    }
    public setScheduleName(scheduleName:string){
        this.scheduleName = scheduleName;
    }
    public setStartDt(startDt:string){
        this.startDt = startDt;
    }
    public setEndDt(endDt:string){
        this.endDt = endDt;
    }
    public setPenyelarasID(penyelarasID:number){
        this.penyelarasID = penyelarasID;
    }
    public setUrusetiaID(urusetiaID:number){
        this.urusetiaID = urusetiaID;
    }  
    public setCourseID(courseID:number){
        this.courseID = courseID;
    }   
    public setStatus(status:string){
        this.status = status;
    } 
}