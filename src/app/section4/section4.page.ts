import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { FormArray, FormBuilder, FormGroup} from '@angular/forms';
import { AlertController, PopoverController } from '@ionic/angular';
import { SidemenuPage } from '../sidemenu/sidemenu.page';
import { Section4 } from '../model/section4.model';
import { Section4Service } from '../services/section4.service';
import { Section } from '../model/section.model';
import { SectionService } from '../services/section.service';

@Component({
  selector: 'app-section4',
  templateUrl: './section4.page.html',
  styleUrls: ['./section4.page.scss'],
})
export class Section4Page implements OnInit {

  section4List : Section4[] = new Array<Section4>();

  topicID:any;
  topicName :any;
  sectionList: Section[];
  section: Section;
  private valueScore : number = 7;
  selectedSection:any={id : 4, name: "Pengurusan Kursus Bahagian I"};


  scheduleID : number = 0;
  public isSubmitted: boolean = false;
  public isRequired: boolean = false;
  private score : number = 7;

  scoreList =[];
  s4Form: FormGroup;
  s4: FormArray;
  
  constructor(private section4Service: Section4Service, private formBuilder: FormBuilder, public alertController: AlertController, private sectionService: SectionService,
    public popoverController: PopoverController, private router: Router) { }
   
    ngOnInit(){    
      this.s4Form = this.formBuilder.group({
        s4: this.formBuilder.array([]),
    });
     
      this.section4Service.getListOfSection4().subscribe(
        data=>{
          this.section4List = data;
          for(let item of this.section4List){
            this.s4 = this.s4Form.get('s4') as FormArray;
            this.s4.push(this.createItem(item));
            //this.scheduleID = item.scheduleID;
            //console.log(data);
          }
          // this.initForm();
        }
      )
     
      this.sectionService.getListOfSection().subscribe(
        data=>{
          this.sectionList = data;
          //this.section = this.sectionList[id];
        }
      )
    }
    
    createItem(item: Section4): FormGroup {
      return this.formBuilder.group({
        id:[item.valSubTopicID],
        section4: [item.valSubTopicName],
        valueScore: 7,
        remarks: ['']  
      });
    }

    get errorControl(){
      return this.s4Form.controls;
    }

    changeRequiredStatus() {
      this.isRequired= true;
    }

    home(){
      this.router.navigateByUrl("/home");
    }

    async presentPopover(ev: any) {
      const popover = await this.popoverController.create({
        component: SidemenuPage,
        cssClass: 'my-custom-class',
        //componentProps:
        event: ev,
        translucent: true
      });
      return await popover.present();
    };

  submit(){
    this.isSubmitted = true;
    this.isRequired = true;
    if (!this.s4Form.valid){
       console.log('Sila isi ulasan...');
       return false;
     }
    else{
      let control = this.s4Form.get('s4') as FormArray;
      let section4List : Section4[] = new Array<Section4>();
  
      for(let i=0; i < control.length; i++){
        let val : Section4 = new Section4();
        
        val.valSubTopicID = control.controls[i].get('id').value;
        
        // if ((control.controls[i].get('valueScore').value != 0) && (control.controls[i].get('valueScore').value <=4))
        // { control.controls[i].get('remarks').value !="";
        //   return false;
        //   //console.log(control.controls[i].get('remarks').value);   
        // }
        val.score = control.controls[i].get('valueScore').value;
        val.remarks = control.controls[i].get('remarks').value;

        section4List.push(val);
      }
      console.log(section4List);  
      this.router.navigateByUrl("/section5");  
     }
   }

   async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Pengesahan',
      message: 'Anda pasti untuk <strong>menghantar penilaian</strong>?',
      buttons: [
        {
          text: 'Tidak',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Ya',
          handler: () => {
            this.isSubmitted = true;
            if (!this.s4Form.valid){
               console.log('Sila isi ruangan yang  diperlukan.');
               alert.dismiss();
               return false;               
             }
             else{
              console.log(this.s4Form.value);  
              this.router.navigateByUrl("/section5");  
             }
            console.log('Confirm Okay');
          }
        }
      ]
    });
    await alert.present();
  }

  }