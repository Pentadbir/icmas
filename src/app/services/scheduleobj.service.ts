import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Topic } from '../model/topic.model';
import { serverEnv } from 'src/environments/server-env';
import { TokenService } from './token.service';
import { Objective } from '../model/obj.model';

@Injectable({
    providedIn: 'root'
})
export class ObjectiveService {

    globalUrl=serverEnv.apiUrl; 
    apiSection1=this.globalUrl + "/section1";

 
    constructor(
        private httpClient: HttpClient,
        private _token : TokenService

    ){}

    // getListOfObj():Observable<any>{
    //     //return this.httpClient.get("/assets/dummydata/obj.json")
    //     return this.httpClient.get(this.apiSection1,this._token.jwtBearer());

    // }

    getListOfObj(newSch:Objective):Observable<any>{

        return this.httpClient.post(this.apiSection1,JSON.stringify({
            scheduleID : newSch.scheduleID,
            course_id : newSch.course_id
        }),this._token.jwtBearer())

    }

    getTopicById(id : string):Observable<Topic>{
        return this.httpClient.get<Topic>("/assets/dummydata/singletopic.json")
    }
}