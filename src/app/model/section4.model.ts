export class Section4 {
    public valTopicID : number;
    public valSubTopicID : number;
    public valSubTopicName : string;
    public compulsoryFlg : number;
    public scheduleID : number;
    public score : number;
    public remarks : string;

    public setValTopicID(valTopicID:number){
        this.valTopicID = valTopicID;
    }
    public setValSubTopicID(valSubTopicID:number){
        this.valSubTopicID = valSubTopicID;
    }
    public setValSubTopicName(valSubTopicName:string){
        this.valSubTopicName = valSubTopicName;
    }    
    public setCompulsoryFlg(compulsoryFlg:number){
        this.compulsoryFlg = compulsoryFlg;
    }
    public setScheduleID(scheduleID:number){
        this.scheduleID = scheduleID;
    }    
    public setScore(score:number){
        this.score = score;
    }
    public setRemarks(remarks:string){
        this.remarks = remarks;
    }
}