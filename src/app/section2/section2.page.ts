import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { FormArray, FormBuilder, FormGroup} from '@angular/forms';
import { AlertController, PopoverController } from '@ionic/angular';
import { SidemenuPage } from '../sidemenu/sidemenu.page';
import { Section2 } from '../model/section2.model';
import { Section2Service } from '../services/section2.service';
import { Section } from '../model/section.model';
import { SectionService } from '../services/section.service';
// import { Skill } from '../model/skill.model';
// import { SkillService } from '../services/skill.service';
import { PickerController } from "@ionic/angular";

@Component({
  selector: 'app-section2',
  templateUrl: './section2.page.html',
  styleUrls: ['./section2.page.scss'],
})
export class Section2Page implements OnInit {

  section2List : Section2[] = new Array<Section2>();
  // skillList : Skill[] = new Array<Skill>();
  topicID:any;
  topicName :any;
  sectionList: Section[];
  section: Section;
  private valueScore : number = 7;
  selectedSection:any={id : 2, name: "Penambahan Pengetahuan, Kemahiran dan Keupayaaan"};
  // selected_range : any;
  // private ranges : string[] = ["0","1","2","3","4","5","6","7"];

  scheduleID : number = 0;
  public isSubmitted: boolean = false;
  public isRequired: boolean = false;
  private score : number = 7;
  private statuseva: any;

  scoreList =[];
  s2Form: FormGroup;
  s2: FormArray;
  
  constructor(private section2Service: Section2Service, private formBuilder: FormBuilder, public alertController: AlertController, public actRoute : ActivatedRoute, private sectionService: SectionService,
    public popoverController: PopoverController, private router: Router,private pickerController: PickerController) { }
   
    ngOnInit(){    
      //utk dapatkan status penilaian dari page home    
      this.statuseva = this.actRoute.snapshot.paramMap.get("status");

      this.s2Form = this.formBuilder.group({
        s2: this.formBuilder.array([]),
    });
     
      this.section2Service.getListOfSection2().subscribe(
        data=>{
          this.section2List = data;
          for(let item of this.section2List){
            this.s2 = this.s2Form.get('s2') as FormArray;
            this.s2.push(this.createItem(item));
            //this.scheduleID = item.scheduleID;
            //console.log(data);
          }
          // this.initForm();
        }
      )
     
      this.sectionService.getListOfSection().subscribe(
        data=>{
          this.sectionList = data;
          //this.section = this.sectionList[id];
        }
      )
    }
    
    createItem(item: Section2): FormGroup {
      return this.formBuilder.group({
        id:[item.valSubTopicID],
        section2: [item.valSubTopicName],
        valueScore: 7,
        remarks: ['']  
      });
    }

    get errorControl(){
      return this.s2Form.controls;
    }

    changeRequiredStatus() {
      this.isRequired= true;
    }

    home(){
      this.router.navigateByUrl("/home");
    }

    async presentPopover(ev: any) {
      const popover = await this.popoverController.create({
        component: SidemenuPage,
        cssClass: 'my-custom-class',
        //componentProps:
        event: ev,
        translucent: true
      });
      return await popover.present();
    };

  submit(){
    this.isSubmitted = true;
    this.isRequired = true;
    if (!this.s2Form.valid){
       console.log('Sila isi ulasan...');
       return false;
     }
    else{
      let control = this.s2Form.get('s2') as FormArray;
      let section2List : Section2[] = new Array<Section2>();
  
      for(let i=0; i < control.length; i++){
        let val : Section2 = new Section2();
        
        val.valSubTopicID = control.controls[i].get('id').value;
        
        if ((control.controls[i].get('valueScore').value != 0) && (control.controls[i].get('valueScore').value <=4))
        { control.controls[i].get('remarks').value !="";
          return false;
          //console.log(control.controls[i].get('remarks').value);   
        }
        val.score = control.controls[i].get('valueScore').value;
        val.remarks = control.controls[i].get('remarks').value;

        section2List.push(val);
      }
      console.log(section2List); 
      this.router.navigateByUrl("/section3");  
     }
    }
  
    async presentAlertConfirm() {
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Pengesahan',
        message: 'Anda pasti untuk <strong>menghantar penilaian</strong>?',
        buttons: [
          {
            text: 'Tidak',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Ya',
            handler: () => {
              this.isSubmitted = true;
              if (!this.s2Form.valid){
                 console.log('Sila isi ruangan yang  diperlukan.');
                 alert.dismiss();
                 return false;               
               }
               else{
                console.log(this.s2Form.value);  
                this.router.navigateByUrl("/section3");  
               }
              console.log('Confirm Okay');
            }
          }
        ]
      });
      await alert.present();
    }


  }