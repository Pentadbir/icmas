import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { Section } from '../model/section.model';
import { Schedule } from '../model/schedule.model';
import { User } from '../model/user.model';
import { SectionService } from '../services/section.service';
//import { AuthService } from '../services/auth.service';
import { ScheduleService } from '../services/schedule.service';



@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.page.html',
  styleUrls: ['./sidemenu.page.scss'],
})
export class SidemenuPage implements OnInit {

  sectionList : Section[] = new Array<Section>();
  
  //sectionList : SectionService[];


  constructor(
    private router: Router, 
    public popoverController: PopoverController,
    private sectionService: SectionService, 
    private sectionSvc: SectionService, 
    private scheduleSvc: ScheduleService, 
    //private authService: AuthService
    ) { }

  ngOnInit() {

    
    this.sectionService.getListOfSection().subscribe(
      success=>{
        console.log(success)
        this.sectionList = success;

      }
    )
   
  }

  

  toSection(){ 
    this.router.navigateByUrl("/section1");
    this.popoverController.dismiss();
  }

  goToSection(sectionID, sectionName) {
    // console.log(id);
    alert('you chose ' + sectionID);
    alert('you chose ' + sectionName);
    this.router.navigateByUrl("/section"+sectionID);
    //this.router.navigateByUrl("/section1"+status+"/"+sectionID+sectionName);
    this.popoverController.dismiss();
  }

  // getByID(id){
  //   for(var i=0; i< (this.data).length; i++)
  //   {
  //     if(this.data[i].code == id)
  //     {
  //       return Promise.resolve(this.data[i]);
  //     }
  //   }


  // toSection5(){
  //   this.router.navigateByUrl("/section5");
  //   this.popoverController.dismiss();
  // }

}
