import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { Section5Page } from './section5.page';

const routes: Routes = [
  {
    path: '',
    component: Section5Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Section5PageRoutingModule {}
