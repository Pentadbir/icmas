import { Component, OnInit } from '@angular/core';
import { ScheduleService } from '../services/schedule.service';
import { Schedule } from '../model/schedule.model';
import { Router, NavigationExtras } from '@angular/router';
import { User } from '../model/user.model';
import { AuthService } from '../services/auth.service';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit{

  userList: User;
  //scheduleList : Schedule[];
  scheduleList : Schedule[] = new Array<Schedule>();
  
  //tukar nama model
  coor :  Schedule[] = new Array<Schedule>();
  constructor(
    private scheduleSvc: ScheduleService, private router: Router, private authService: AuthService ) {}

  ngOnInit(){
    this.scheduleSvc.getHome().subscribe(
      success=>{
      //  let slist : Schedule = success.
      //  console.log(slist);
        this.scheduleList= success[0].schedules;
        this.coor= success[0].schedule;
        // console.log(this.coor); 


      }
   )

    this.scheduleSvc.getListOfSchedule().subscribe(
      data=>{
        //this.scheduleList= data;
      }
    )

    this.authService.getUser().subscribe(
      data=>{
        this.userList= data;
      }
    )
    //this.scheduleList = this.scheduleSvc.scheduleList;
    //console.log('test');
    
    //this.scheduleSvc.getListOfSchedule().subscribe((data)=>{
      //console.log(data);
      //for (const i in data){
      //  console.log(i);
       // this.scheduleList. = data; 
      //}
    //})

  }

  toSection(sch){
     let navigateExtra : NavigationExtras = {
       state:{
         user :sch
       }
     }
    this.router.navigate(["/section1"],navigateExtra);
    console.log(navigateExtra)

  }  

  logOut() {
    // cleans out data and sets login page as root:
    // this.authService.logout().subscribe(
    // data=>{
    //   console.log(data);
    // },
    // error =>{
    //   console.log(error);
    // },
    // // does navigate user
    // () => {
      console.log("bye");
      this.router.navigateByUrl("/login");
    // }      
    // );

  }

}
