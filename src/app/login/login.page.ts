import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../model/user.model';
import { AuthService } from '../services/auth.service';
import { getLocaleMonthNames } from '@angular/common';
import { observable } from 'rxjs';
import { ScheduleService } from '../services/schedule.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

// class Validators{
//   static required(control:AbstractControl): ValidationErrors | null
//   static minLength (minLength:number): ValidationErrors | null
// } 

export class LoginPage implements OnInit {

  loginForm : FormGroup;
  public isSubmitted: boolean = false;
  //isSubmitted= false;

  constructor(
    private formBuilder: FormBuilder,
    private router: Router, private authSvc: AuthService,
    private schedulesvc: ScheduleService
    ) { }
  
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username:['',  Validators.required],
      password:['',  Validators.required]
    })

    // this.getHome()
  }

  get errorControl(){
    return this.loginForm.controls;
  }

  submitForm(){
        
        let username=this.loginForm.controls['username'].value;
        let password=this.loginForm.controls['password'].value;
        let newUser:User=new User();

        newUser.newICNo=username;
        newUser.passwd=password;

        this.authSvc.login(newUser).subscribe(
          suc=>{
            console.log('done login')
            console.log(suc);
            this.authSvc.setToken(suc.access_token)
            this.router.navigateByUrl("/home");  
          },
          err=>{
            alert('Pengguna tidak wujud');
            //{pop};
          }
         
        )
              
      // }
  }
     getHome(){
         this.schedulesvc.getHome().subscribe(
           success=>{
            console.log(success); 
           }
        )
      }
  // login(){  
  //   pengguna : Peserta = new Peserta();
  //   pengguna.setUserName(this.loginForm.controls['username'].value);
  //   pengguna.setPassword(this.loginForm.controls['password'].value);

  //   this.authService.login(this.username).subscribe(
  //     res=>{
  //       console.log(res);
  //       this.router.navigateByUrl("/home");
  //     },
  //     err=>{
  //       console.log("error", err);
  //       alert ("Salah kombinasi No KP dan Kata Laluan.");     
  //     },
  //     () =>{
  //       //this.dismissLogin();
  //      // this.navCltr.navigateRoot('/section1');
  //     }
  //   );
  // }


  // submit(loginForm){
  //   let pengguna : User = new User();
  //   pengguna.setUserName(this.loginForm.controls['username'].value);
  //   pengguna.setPassword(this.loginForm.controls['password'].value);

  //   console.log(this.loginForm.value);
  //   this.authService.login(pengguna).subscribe(
  //     res=>{
  //       console.log(res);
  //       this.router.navigateByUrl("/home");
  //     },
  //     err=>{
  //       console.log("error", err);
  //       alert ("Salah kombinasi No KP dan Kata Laluan.");     
  //     },
  //   );
  //   //this.router.navigateByUrl("/home");
  // }

  // Dismiss Login Modal
  // dismissLogin() {
  //   this.modalController.dismiss();
  // }
  
}