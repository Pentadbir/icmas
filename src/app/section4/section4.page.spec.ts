import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { Section4Page } from './section4.page';

describe('Section4Page', () => {
  let component: Section4Page;
  let fixture: ComponentFixture<Section4Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Section4Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(Section4Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
