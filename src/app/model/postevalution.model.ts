export class postEvalution {
    public topicID :number
    public subTopicID : number;
    public cddID : number;
    public scheduleID : number;
    public evaVal : number;
    public rem : string;
    public evaID : number;
    //public typeVal : string;

    public setTopicID(topicID:number){
        this.topicID = topicID;
    }
    public setSubTopicID(subTopicID:number){
        this.subTopicID = subTopicID;
    }
    public setCddID(cddID:number){
        this.cddID = cddID;
    }
    public setScheduleID(scheduleID:number){
        this.scheduleID = scheduleID;
    }
    public setEvaVal(evaVal:number){
        this.evaVal = evaVal;
    }
    public setRem(rem:string){
        this.rem = rem;
    }
    public setEvaID(evaID:number){
        this.evaID = evaID;
    }
    // public setTypeVal(typeVal:string){
    //     this.typeVal = typeVal;
    // }
}