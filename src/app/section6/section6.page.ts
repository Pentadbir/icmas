import { Component, OnInit } from '@angular/core';
import { FormArray, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertController, PopoverController } from '@ionic/angular';
import { SidemenuPage } from '../sidemenu/sidemenu.page';
import { Router } from '@angular/router';
import { Section6 } from '../model/section6.model';
import { Section6Service } from '../services/section6.service';

@Component({
  selector: 'app-section6',
  templateUrl: './section6.page.html',
  styleUrls: ['./section6.page.scss'],
})
export class Section6Page implements OnInit {

  scheduleID : number = 0;
  public isSubmitted: boolean = false;
  public isRequired: boolean = false;
  private score : number = 7;
  selectedSection:any={id : 6, name: "Faedah Kursus"};
  s6Form: FormGroup;

  constructor(private section6Service: Section6Service, private formBuilder: FormBuilder, public alertController: AlertController, private router: Router, public popoverController: PopoverController) { }

  ngOnInit() {
    this.s6Form = this.formBuilder.group({
      remarks1:["", Validators.required],
      remarks2:["", Validators.required]
    });

  }

  get errorControl(){
    return this.s6Form.controls;
  }

  async presentPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: SidemenuPage,
      cssClass: 'my-custom-class',
      //componentProps: 
      event: ev,
      translucent: true
    });
    return await popover.present();
  }

  submit(){
    this.isSubmitted = true;
    if (!this.s6Form.valid){
       console.log('Please provide all the required values...');
       return false;
     }
     else{
      console.log(this.s6Form.value);  
      this.router.navigateByUrl("/home");  
     }
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Pengesahan',
      message: 'Anda pasti untuk <strong>menghantar penilaian</strong>?',
      buttons: [
        {
          text: 'Tidak',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Ya',
          handler: () => {
            this.isSubmitted = true;
            if (!this.s6Form.valid){
               console.log('Sila isi ruangan yang  diperlukan.');
               alert.dismiss();
               return false;               
             }
             else{
              console.log(this.s6Form.value);  
              this.router.navigateByUrl("/home");  
             }
            console.log('Confirm Okay');
          }
        }
      ]
    });
    await alert.present();
  }

}
