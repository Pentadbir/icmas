import { Injectable} from "@angular/core"
import { Schedule} from "../model/schedule.model"
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs'


@Injectable({
    providedIn : 'root'
})

export class ScheduleService{
    welcomeTxt: string ="Hello Service World"

    constructor(
        private httpClient : HttpClient
    ){}

    getScheduleListFromApi(username: number):Observable<any>{
        return this.httpClient.get("http://localhost/myICMASPenilaian/src/app/api/login.php"+username)
    }

    getScheduleFromApi(scheduleid: number):Observable<any>{
        return this.httpClient.get("https://reqres.in/api/users/"+scheduleid)
        //return this.httpClient.get("http://localhost/myICMASPenilaian/src/app/api/login.php/"+user_id)

    }

    getGreetings(){
        return this.welcomeTxt
    }

    getScheduleList(){
            let ScheduleList = new Array<Schedule>();
            for(let i=0;i <10; i++){
                let schedule =new Schedule();
                schedule.setScheduleName("Kursus " +i);
                schedule.setScheduleID(i);
                ScheduleList.push(schedule)
            }
            return ScheduleList;
    }

}