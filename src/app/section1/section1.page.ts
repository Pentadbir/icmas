import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import { AlertController, PopoverController } from '@ionic/angular';
import { SidemenuPage } from '../sidemenu/sidemenu.page';
import { Objective } from '../model/obj.model';
import { ObjectiveService } from '../services/scheduleobj.service';
import { Section } from '../model/section.model';
import { Schedule } from '../model/schedule.model';
import { SectionService } from '../services/section.service';
import { RemarksValidator } from '../validators/remark';
//import { AuthService } from '../services/auth.service';
import { ScheduleService } from '../services/schedule.service';


@Component({
  selector: 'app-section1',
  templateUrl: './section1.page.html',
  styleUrls: ['./section1.page.scss'],
})
export class Section1Page implements OnInit {

  objList : Objective[] = new Array<Objective>();
  // skillList : Skill[] = new Array<Skill>();
  topicID:any;
  topicName :any;
  sectionList: Section[];
  section: Section;
  private valueScore : number = 7;
  selectedSection:any={id : 1, name: "Objektif Kursus"};
  // selected_range : any;
  // private ranges : string[] = ["0","1","2","3","4","5","6","7"];
  scheduleList : Schedule[] = new Array<Schedule>();
  scheduleID : number = 0;
  public isSubmitted: boolean = false;
  public isRequired: boolean = false;
  public required = "";
  private score : number = 7;
  private statusEva: any;
  private sectionID: any;

  scoreList =[];
  s1Form: FormGroup;

  sch: any;
  courseID: any;

  // s4Form: FormGroup;
 
  s1: FormArray;

  
  constructor(private objectiveService: ObjectiveService, 
    private formBuilder: FormBuilder, 
    private sectionService: SectionService,
    private scheduleSvc: ScheduleService, 
    public popoverController: PopoverController, 
    private router: Router,
    private actRoute: ActivatedRoute) { 
      
        
    }
   
    ngOnInit(){  

      this.actRoute.queryParams.subscribe(params =>
        {
          if(this.router.getCurrentNavigation().extras.state){
          this.sch = this.router.getCurrentNavigation().extras.state.user;
          this.scheduleID = this.router.getCurrentNavigation().extras.state.user.scheduleID;
          this.courseID = this.router.getCurrentNavigation().extras.state.user.courseID;

          //inform norna to add -- this for status checking
          //this.statusPenilaan = this.router.getCurrentNavigation().extras.state.user.status.statusPenilaan;

        }})

        
      this.s1Form = this.formBuilder.group({
        s1: this.formBuilder.array([]),
        remarksAll : [""]
      });
     //this.setRemarksValidators();

     let newSch:Objective=new Objective();
     newSch.scheduleID=this.scheduleID;
     newSch.course_id=this.courseID;

      this.objectiveService.getListOfObj(newSch).subscribe(
        success=>{
          console.log(success);

          this.objList = success;
          for(let item of this.objList){
            this.s1 = this.s1Form.get('s1') as FormArray;
            this.s1.push(this.createItem(item));
            this.scheduleID = item.scheduleID;
            //console.log(data);
          }
          // this.initForm();
        }
      )
      
      // this.sectionService.getListOfSection().subscribe(
      //   data=>{
      //     this.sectionList = data;
      //     //this.section = this.sectionList[id];
      //   }
      // )
    }
    
    createItem(item: Objective): FormGroup {
      return this.formBuilder.group({
        id:[item.scheduleobj_id],
        objective: [item.scheduleobj_text],
        valueScore: [7],
        remarks: ['']
        //remarks: ['', RemarksValidator.remarksRequired ] 
   
      });
    }

    // setRemarksValidators() {
    //   const remarksControl = this.s1Form.get('remarks').value;
    //   const scoreControl = this.s1Form.get('valueScore').value;
  
    //   this.s1Form.get('valueScore').value
    //     .subscribe(score => {
  
    //       if (score  <= 4) {
    //         remarksControl.setValidators([Validators.required]);
    //       }
  
    //       if (score == 0) {
    //         remarksControl.setValidators(null);
    //       }
  
    //       remarksControl.updateValueAndValidity();
    //     });
    // }

    get errorControl(){
      return this.s1Form.controls;
    }

    home(){
      this.router.navigateByUrl("/home");
    }

    async presentPopover(ev: any) {
      const popover = await this.popoverController.create({
        component: SidemenuPage,
        cssClass: 'my-custom-class',
        //componentProps:
        event: ev,
        translucent: true
      });
      return await popover.present();
    };

    addRangeVal(val) {    
      if (this.valueScore == 7){
        this.valueScore = 0;
      }else{
        this.valueScore++;
      } 
      //this.name.setValue('Nancy');
        //
      //control.push(this.itemRows()his.scoreList.push(this.valueScore);
        //console.log(this.valueScore);
    }
    
    subtractRangeVal(val) {
      if (this.valueScore == 0){
        this.valueScore = 7;
      }else{
          this.valueScore--;
      }
      console.log(this.valueScore);
      return this.valueScore;
      // this.scoreList.splice(1);
      
    }

  submit(){

      let control = this.s1Form.get('s1') as FormArray;
      let objList : Objective[] = new Array<Objective>();
  
      for(let i=0; i < control.length; i++){
        let val : Objective = new Objective();
        
        val.scheduleobj_id = control.controls[i].get('id').value;
        if ((control.controls[i].get('valueScore').value != 0) && (control.controls[i].get('valueScore').value <=4) && (control.controls[i].get('remarks').value == ""))
        { 
          return false;
        }
        else{
        this.isSubmitted = true;
        val.score = control.controls[i].get('valueScore').value;
        val.remarks = control.controls[i].get('remarks').value;
        val.scheduleID = this.scheduleID;
        objList.push(val);
        this.router.navigateByUrl("/section2"); 
        }
      }
      console.log(objList); 
      //this.router.navigateByUrl("/section2"); 
   
    }

    async presentAlertConfirm() {
      const alert = await this.alertController.create({
        cssClass: 'my-custom-class',
        header: 'Pengesahan',
        message: 'Anda pasti untuk <strong>menghantar penilaian</strong>?',
        buttons: [
          {
            text: 'Tidak',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              console.log('Confirm Cancel: blah');
            }
          }, {
            text: 'Ya',
            handler: () => {
              this.isSubmitted = true;
              if (!this.s1Form.valid){
                 console.log('Sila isi ruangan yang  diperlukan.');
                 alert.dismiss();
                 return false;               
               }
               else{
                console.log(this.s1Form.value);  
                this.router.navigateByUrl("/section2");  
               }
              console.log('Confirm Okay');
            }
          }
        ]
      });
      await alert.present();
    }
    // changeRequiredStatus() {
    //   if (this.valueScore == 0 && this.valueScore <= 4)
    //   {}
    // }
  }