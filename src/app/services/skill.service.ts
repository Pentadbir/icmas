import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Topic } from '../model/topic.model';

@Injectable({
    providedIn: 'root'
})
export class SkillService {
 
    constructor(
        private httpClient: HttpClient
    ){}

    getListOfSkill():Observable<any>{
        return this.httpClient.get("/assets/dummydata/skill.json")
    }

    getTopicById(id : string):Observable<Topic>{
        return this.httpClient.get<Topic>("/assets/dummydata/singletopic.json")
    }
}