export class Lect {
    public lecID : number;
    public lecName : string;
    public actID : number;
    public startTime: string;
    public endTime: string;
    public lecTyp : string;
    public lecStt : string;
    public lect_courseID : number;

    public setLecID(lecID:number){
        this.lecID = lecID;
    }
    public setName(lecName:string){
        this.lecName = lecName;
    }
    public setActID(actID:number){
        this.actID = actID;
    }
    public setStartTime(startTime:string){
        this.startTime = startTime;
    }
    public setEndTime(endTime:string){
        this.endTime = endTime;
    }
    public setLecTyp(lecTyp:string){
        this.lecTyp = lecTyp;
    }   
    public setLecStt(lecStt:string){
        this.lecStt = lecStt;
    }  
    public setCourseID(lect_courseID:number){
        this.lect_courseID = lect_courseID;
    }  
}