import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'evaluation',
    loadChildren: () => import('./evaluation/evaluation.module').then( m => m.EvaluationPageModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'section5',
    loadChildren: () => import('./section5/section5.module').then( m => m.Section5PageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'sidemenu',
    loadChildren: () => import('./sidemenu/sidemenu.module').then( m => m.SidemenuPageModule)
  },
  //inform norna 
  {
    path: 'section1/:status',
    loadChildren: () => import('./section1/section1.module').then( m => m.Section1PageModule)
  },
  {
    path: 'section6',
    loadChildren: () => import('./section6/section6.module').then( m => m.Section6PageModule)
  },
  {
    path: 'section2',
    loadChildren: () => import('./section2/section2.module').then( m => m.Section2PageModule)
  },
  {
    path: 'section3',
    loadChildren: () => import('./section3/section3.module').then( m => m.Section3PageModule)
  },
  {
    path: 'section4',
    loadChildren: () => import('./section4/section4.module').then( m => m.Section4PageModule)
  },
  {
    path: 'section5-in',
    loadChildren: () => import('./section5-in/section5-in.module').then( m => m.Section5InPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
