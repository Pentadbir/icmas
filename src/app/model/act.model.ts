import { Lect } from "./lect.model";

export class Act {
    public actID : number;
    public topicNo : number;
    public topicName : string;
    public sideActID : number;
    public startTime : string; 
    public endTime: string;
    public continueFlg : string;
    public lecList : Lect[];
    public scheduleID: number;

    public setActID(actID:number){
        this.actID = actID;
    }
    public setTopicNo(topicNo:number){
        this.topicNo = topicNo
    }
    public setTopicName(topicName:string){
        this.topicName = topicName;
    }
    public setSideActID(sideActID:number){
        this.sideActID = sideActID;
    }    
    public setStartTime(startTime:string){
        this.startTime = startTime;
    }
    public setEndTime(endTime:string){
        this.endTime = endTime;
    }
    public setContinueFlg(continueFlg:string){
        this.continueFlg = continueFlg;
    }   
     
}