import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { Section5PageRoutingModule } from './section5-routing.module';

import { Section5Page } from './section5.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    Section5PageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [Section5Page]
})
export class Section5PageModule {}
