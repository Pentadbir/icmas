export class Section {
    public sectionID : number;
    public sectionName : string;
    // public sectionCode : string;
    // public maxGrd : number;
    // public typeVal : string;

    public setSectionID(sectionID:number){
        this.sectionID = sectionID;
    }
    public setSectionName(sectionName:string){
        this.sectionName = sectionName;
    }
    // public setSectionCode(sectionCode:string){
    //     this.sectionCode = sectionCode;
    // }    
    // public setMaxGrd(maxGrd:number){
    //     this.maxGrd = maxGrd;
    // }
    // public setTypeVal(typeVal:string){
    //     this.typeVal = typeVal;
    // }
}