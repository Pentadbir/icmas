import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class TokenService {

	public TOKEN_KEY = "jwticmas";

	constructor() { }

	public isTokenExpired(offsetSeconds? : number): boolean {

		let token: string = localStorage.getItem(this.TOKEN_KEY);

		if(token === null || token === '') {
			return true;
		}

		let date = this.getTokenExpirationDate(token);
		offsetSeconds = offsetSeconds || 0;

		if(date === null) {
			return true;
		}

		return !(date.valueOf() > new Date().valueOf() + offsetSeconds * 1000);
	}

	public getTokenExpirationDate(token: string):Date | null {

		let decode: any;
		decode = this.decodeExpToken(token);

		if(!decode.hasOwnProperty('exp')) {
			return null;
		}

		const date = new Date(0);
		date.setUTCSeconds(decode.exp);

		return date;
	}

	public decodeExpToken(token: string): any {

		if(token === null) {
			return null;
		}

		let parts = token.split('.');

		if(parts.length !== 3) {
			throw new Error('Token yang diterima bukan dalam format JWT!');
		}

		var splitToken = token.split('.')[1];

		var base64 = splitToken.replace('-', '+').replace('_', '/');

		var decoded = JSON.parse(window.atob(base64))

		if(!window.atob(base64)) {
			throw new Error('Token tidak boleh didecode!');
		}

		return decoded;
	}

	public jwtBearer() {
		let jwt = {
			headers: new HttpHeaders({
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + localStorage.getItem(this.TOKEN_KEY) 
			})
		};
		return jwt;
	}
}